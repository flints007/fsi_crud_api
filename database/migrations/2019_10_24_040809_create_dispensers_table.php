<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDispensersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispensers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('hs_code'); //84131100 
            $table->string('model_no'); //JDK50D222N
            $table->string('magnitude_of_discharge'); //Standard
            $table->string('operation_mode'); // Normal
            $table->string('oil_transportation_mode'); //Self Priming
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispensers');
    }
}
