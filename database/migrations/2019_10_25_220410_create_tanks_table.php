<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
 
        Schema::create('tanks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('tank_type');
            $table->string('stored_fuel');
            $table->integer('liter'); 
            $table->unsignedBigInteger('dispenser_id')->nullable();  // assuming 1 tank is phyicaly mapped to 1 dispenser
            $table->foreign('dispenser_id')->references('id')->on('dispensers')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tanks');
    }
}
