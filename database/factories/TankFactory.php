<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Tank;
use Faker\Generator as Faker;

$factory->define(Tank::class, function (Faker $faker) {
    return [
        //
    'name' => $faker->name,
	'tank_type' => $faker->name,
	'stored_fuel' => $faker->name,
	'liter' => 534,
	'dispenser_id' => factory('App\Tank')->create()->id,
    ];
});
