<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\StationTransaction;
use Faker\Generator as Faker;

$factory->define(StationTransaction::class, function (Faker $faker) {
    return [
     'user_id' => factory('App\User')->create()->id,
	'tank_id' => factory('App\Tank')->create()->id,
	'vol_sold_by_dispenser' =>'234324',
	'transaction_type' =>'Delivery' ,
	'new_product' => '234324',
    ];
});
