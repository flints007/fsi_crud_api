<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Dispenser;
use Faker\Generator as Faker;

$factory->define(Dispenser::class, function (Faker $faker) {
    return [ 
    'name' => $faker->name,
	'hs_code' => $faker->sentence,
	'model_no' => $faker->name,
	'magnitude_of_discharge' => 'Standard',
	'operation_mode' => 'Normal',
	'oil_transportation_mode' => 'Gas',
 
    ];
});

 