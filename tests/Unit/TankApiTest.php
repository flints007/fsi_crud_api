<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Auth;
use App\Tank; 
class TankApiTest extends TestCase
{


    
          /** @test */
    public function test_a_user_can_read_all_the_tanks()
	    {
	        //Given we have Tank in the database
	       // $tank = factory('App\Tank')->create();

	        //When user visit the tank route
	        $response = $this->get('/api/v1/tanks')
	        ->assertStatus(201);
	        $this->assertTrue(true);
	        //He should be able to read the task
	        $response->assertSee($tank->name);
	    }

	    	    /** @test */
		public function authenticated_users_can_create_a_new_tank()
		{
		    //Given we have an authenticated user
		   // $this->actingAs(factory('App\User')->create());
		    //And a task object
		    $data = [

		    	];

	 
		    $tank = factory('App\Tank')->make();
		    //When user submits post request to create tank endpoint
		    $this->post('api/v1/transactions',$tank->toArray());
		    //It gets stored in the database
		    $this->assertEquals(1,Tank::all()->count());
		}


		/** @test */
		public function a_dispenser_requires_a_tank_type(){

		   // $this->actingAs(factory('App\User')->create());

		    $tank = factory('App\Tank')->make(['tank_type' => null]);

		    $this->post('api/v1/tanks',$tank->toArray())
		            ->assertSessionHasErrors('tank_type');
		}

		/** @test */
		public function a_task_requires_a_stored_fuel(){

		   // $this->actingAs(factory('App\User')->create());

		    $tank = factory('App\Tank')->make(['stored_fuel' => null]);

		    $this->post('api/v1/tanks',$tank->toArray())
		        ->assertSessionHasErrors('stored_fuel');
		}

		/** @test */
		public function a_task_requires_a_liter(){

		   // $this->actingAs(factory('App\User')->create());

		    $tank = factory('App\Tank')->make(['liter' => null]);

		    $this->post('api/v1/tanks',$tank->toArray())
		        ->assertSessionHasErrors('liter');
		}
 
 

				/** @test */
		public function user_can_update_the_dispenser(){

		    //Given we have a signed in user
		   // $this->actingAs(factory('App\User')->create());
		    //And a task which is created by the user
		    $tank = factory('App\Tank')->create();
		    $tank->model_no = "Terseer";
		    //When the user hit's the endpoint to update the task
		    $this->put('api/v1/tanks'.$tank->id, $tank->toArray());
		    //The task should be updated in the database.
		    $this->assertDatabaseHas('tanks',['id'=> $tank->id , 'model_no' => 'Terseer']);

		}



				/** @test */
		public function user_can_delete_the_dispenser(){

		    //Given we have a signed in user
		   // $this->actingAs(factory('App\User')->create());
		    //And a task which is created by the user
		    $tank = factory('App\Tank')->create();
		    //When the user hit's the endpoint to delete the task
		    $this->delete('api/v1/tanks'.$tank->id);
		    //The task should be deleted from the database.
		    $this->assertDatabaseMissing('tanks',['id'=> $tank->id]);

		}


}
