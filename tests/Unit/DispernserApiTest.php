<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Dispenser;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Auth;


class DispernserApiTest extends TestCase
{
	use WithoutMiddleware;

 

          /** @test */
    public function test_a_user_can_read_all_the_dispensers()
	    {
	        //Given we have Dispenser in the database
	        $dispenser = factory('App\Dispenser')->create();

	        //When user visit the dispenser route
	        $response = $this->get('/api/v1/dispensers')
	        ->assertStatus(201);
	        $this->assertTrue(true);
	        //He should be able to read the task
	        $response->assertSee($dispenser->name);
	    }


	    /** @test */
		public function authenticated_users_can_create_a_new_dispenser()
		{
		    //Given we have an authenticated user
		    $this->actingAs(factory('App\User')->create());
		    //And a task object
		    $dispenser = factory('App\Dispenser')->make();
		    //When user submits post request to create dispenser endpoint
		    $this->post('api/v1/dispensers',$dispenser->toArray());
		    //It gets stored in the database
		    $this->assertEquals(1,Dispenser::all()->count());
		}



		/** @test */
		// public function unauthenticated_users_cannot_create_a_new_dispenser()
		// {
		//     //Given we have a Dispenser object
		//     $dispenser = factory('App\Dispenser')->make();
		//     //When unauthenticated user submits post request to create dispenser endpoint
		//     // He should be redirected to login page
		//     $this->post('api/v1/dispensers',$dispenser->toArray())
		//          ->assertRedirect('login');
		// }


		/** @test */
		public function a_dispenser_requires_a_name(){

		    $this->actingAs(factory('App\User')->create());

		    $dispenser = factory('App\Dispenser')->make(['name' => null]);

		    $this->post('api/v1/dispensers',$dispenser->toArray())
		            ->assertSessionHasErrors('name');
		}

		/** @test */
		public function a_task_requires_a_hs_code(){

		    $this->actingAs(factory('App\User')->create());

		    $dispenser = factory('App\Dispenser')->make(['hs_code' => null]);

		    $this->post('api/v1/dispensers',$dispenser->toArray())
		        ->assertSessionHasErrors('hs_code');
		}

		/** @test */
		public function a_task_requires_a_model_no(){

		    $this->actingAs(factory('App\User')->create());

		    $dispenser = factory('App\Dispenser')->make(['model_no' => null]);

		    $this->post('api/v1/dispensers',$dispenser->toArray())
		        ->assertSessionHasErrors('model_no');
		}

		/** @test */
		public function a_task_requires_a_magnitude_of_discharge(){

		    $this->actingAs(factory('App\User')->create());

		    $dispenser = factory('App\Dispenser')->make(['magnitude_of_discharge' => null]);

		    $this->post('api/v1/dispensers',$dispenser->toArray())
		        ->assertSessionHasErrors('magnitude_of_discharge');
		}

		/** @test */
		public function a_task_requires_an_operation_mode(){

		    $this->actingAs(factory('App\User')->create());

		    $dispenser = factory('App\Dispenser')->make(['operation_mode' => null]);

		    $this->post('api/v1/dispensers',$dispenser->toArray())
		        ->assertSessionHasErrors('operation_mode');
		}

		/** @test */
		public function a_task_requires_an_oil_transportation_mode(){

		    $this->actingAs(factory('App\User')->create());

		    $dispenser = factory('App\Dispenser')->make(['oil_transportation_mode' => null]);

		    $this->post('api/v1/dispensers',$dispenser->toArray())
		        ->assertSessionHasErrors('oil_transportation_mode');
		}


				/** @test */
		public function user_can_update_the_dispenser(){

		    //Given we have a signed in user
		    $this->actingAs(factory('App\User')->create());
		    //And a task which is created by the user
		    $dispenser = factory('App\Dispenser')->create();
		    $dispenser->model_no = "Terseer";
		    //When the user hit's the endpoint to update the task
		    $this->put('api/v1/dispensers'.$dispenser->id, $dispenser->toArray());
		    //The task should be updated in the database.
		    $this->assertDatabaseHas('dispensers',['id'=> $dispenser->id , 'model_no' => 'Terseer']);

		}



				/** @test */
		public function user_can_delete_the_dispenser(){

		    //Given we have a signed in user
		    $this->actingAs(factory('App\User')->create());
		    //And a task which is created by the user
		    $dispenser = factory('App\Dispenser')->create();
		    //When the user hit's the endpoint to delete the task
		    $this->delete('api/v1/dispensers'.$dispenser->id);
		    //The task should be deleted from the database.
		    $this->assertDatabaseMissing('dispensers',['id'=> $dispenser->id]);

		}
 



}
