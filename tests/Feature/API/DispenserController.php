<?php

namespace Tests\Feature\API;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DispenserController extends TestCase
{
 

        /** @test */
        public function userCanListAllDispensers()
            {
                $response = $this->get(route('/api/v1/dispensers'));   
                // assertSuccessful
                $response->assertSuccessful();
                // assertViewIs
                $response->assertViewIs('dispensers.index');
                // assertViewHas
                $response->assertViewHas('dispensers');
            }

 
        /** @test */
        public function usersCanCreateANewDispenser()
        {
            //Given we have an authenticated user
            $this->actingAs(factory('App\User')->create());
            //And a task object
            $dispenser = factory('App\Dispenser')->make();
            //When user submits post request to create dispenser endpoint
            $this->post('api/v1/dispensers',$dispenser->toArray());
            //It gets stored in the database
            $this->assertEquals(1,Dispenser::all()->count());
        }

                 /** @test */
        public function userCanViewSingleDispenser(){

            //Given we have a signed in user
            $this->actingAs(factory('App\User')->create());
            //And a task which is created by the user
            $dispenser = factory('App\Dispenser')->create();
            //When the user hit's the endpoint to delete the task
            $this->get('api/v1/dispensers'.$dispenser->id);
            //The task should be deleted from the database.
            $this->assertDatabaseMissing('dispensers',['id'=> $dispenser->id]);

        } 
 
                /** @test */
        public function user_can_update_the_dispenser(){

            //Given we have a signed in user
            $this->actingAs(factory('App\User')->create());
            //And a task which is created by the user
            $dispenser = factory('App\Dispenser')->create();
            $dispenser->model_no = "Terseer";
            //When the user hit's the endpoint to update the task
            $this->put('api/v1/dispensers'.$dispenser->id, $dispenser->toArray());
            //The task should be updated in the database.
            $this->assertDatabaseHas('dispensers',['id'=> $dispenser->id , 'model_no' => 'Terseer']);

        }


                /** @test */
        public function userCanDeleteDispenser(){

            //Given we have a signed in user
            $this->actingAs(factory('App\User')->create());
            //And a task which is created by the user
            $dispenser = factory('App\Dispenser')->create();
            //When the user hit's the endpoint to delete the task
            $this->delete('api/v1/dispensers'.$dispenser->id);
            //The task should be deleted from the database.
            $this->assertDatabaseMissing('dispensers',['id'=> $dispenser->id]);

        }
 
}
