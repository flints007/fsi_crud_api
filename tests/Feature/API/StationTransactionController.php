<?php

namespace Tests\Feature\API;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StationTransactionController extends TestCase
{
   


         /** @test */
    public function UserUploadDailyTransaction(){
        //Given we have a signed in user
        $this->actingAs(factory('App\User')->create());
         //And a task which is not created by the user
        $tank = factory('App\Tank')->create();
        $data = [

                    'user_id' => Auth::user()->id,
                    'tank_id' => $tank->id, 
                    'vol_left_in_tank' => '32234',
                    'vol_sold_by_dispenser' => '2343', 
                    'transaction_type' =>'End Day',
                    'new_product' => '324',

        ];
        
        $this->post('/api/v1/transactions',$data);
        $transactions = $this->get('/api/v1/transactions');  

        $this->assertDatabaseMissing('transactions',['id'=> $task->id]);
    }

 
}
