<?php

namespace Tests\Feature\API;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TankController extends TestCase
{
 


    /** @test */
    public function userCanListAllDispensers()
        {
             $tank = factory('App\Tank')->create();
            $response = $this->get(route('/api/v1/tank'));      
            $response->assertSuccessful();
            $response->assertViewIs('tanks.index');
            $response->assertViewHas('tanks');
        }


          /** @test */
        public function userCanCreateANewTank()
        {
            //Given we have an authenticated user
            $this->actingAs(factory('App\User')->create());
            //And a task object
            $tank = factory('App\Tank')->make();
            //When user submits post request to create tank endpoint
            $this->post('api/v1/tanks',$tank->toArray());
            //It gets stored in the database
            $this->assertEquals(1,Tank::all()->count());
        }
  

     /** @test */
    public function testUserViewSinlgeTank(){
        //Given we have a signed in user
        $this->actingAs(factory('App\User')->create());
        //And a task which is not created by the user
        $tank = factory('App\Tank')->create();
        //When the user hit's the endpoint to get the task
         $this->get('/api/v1/tanks'.$tank->id);
         //The task should be deleted from the database.
        $this->assertDatabaseMissing('tanks',['id'=> $task->id]);
    }

 

        /** @test */
    public function testUserUpdateTask(){
        //Given we have a signed in user
        $this->actingAs(factory('App\User')->create());
        //And a task which is not created by the user
        $tank = factory('App\Task')->create();
        $tank->name = "Updated Title";
    
        $this->put('/api/v1/tanks'.$tank->id, $tank->toArray());
        //The task should be updated in the database.
        $this->assertDatabaseHas('tanks',['id'=> $tank->id , 'name' => 'Updated Title']);
    }



     /** @test */
    public function testUserDeleteTask(){
        //Given we have a signed in user
        $this->actingAs(factory('App\User')->create());
        //And a task which is not created by the user
        $tank = factory('App\Tank')->create();
        //When the user hit's the endpoint to delete the task
         $this->delete('/api/v1/tanks'.$tank->id);
         //The task should be deleted from the database.
        $this->assertDatabaseMissing('tanks',['id'=> $task->id]);
    }

 

}
