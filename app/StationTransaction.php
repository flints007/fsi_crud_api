<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\DeliveryUtils;
use App\Traits\DelivertUtils;

class StationTransaction extends Model
{

    use DelivertUtils;

       protected $fillable = [

        'user_id', 'tank_id', 'vol_left_in_tank', 'vol_sold_by_dispenser', 'transaction_type'
    ];


    public function tank()
    {
          return $this->belongsTo('App\Tank');
       
    }

        public function user()
    {
          return $this->belongsTo('App\User');
       
    }
 
   /**
        this method will return the vol_left_in_tank 
     */

    public function getVolLeftInTank()
    {
        return $this->vol_left_in_tank;
    }

   /**
        this method will return the vol_sold_by_dispenser 
     */
    public function getVolSoldByDispenser()
    {
        return $this->vol_sold_by_dispenser;
    }


     public function setVolSoldByDispenser($vol_sold_by_dispenser)
    {
       return $this->vol_sold_by_dispenser = $vol_sold_by_dispenser;
    }



   /**
        this method will return the day form created_at field 
     */
    public function getCurrentDay()
    {
        
    return $this->created_at;
    }

 


   /**
        this method will return the day form created_at field 
     */

    public static function validateCurrentVolumnISLessThanPrevious($current_vol_left, $transaction_type)
    { 

    $previous_vol_lfet = DB::table('station_transactions')->whereDay('created_at', '=', date('d') - 1)->first();   // get the previous_vol_lfet from the tank. While   is the current volume left  
     
     if ($transaction_type == 'Delivery') {
        
        $check_current_vol_left_is_less = false;
        // add previous_vol_lfet with  current_vol_left and update the vol available in tank 
     }else{

        $check_current_vol_left_is_less = ($current_vol_left < $previous_vol_lfet->vol_left_in_tank ? true : false); 
     } 

     return  $check_current_vol_left_is_less;

    }


}
