<?php


namespace App;


use Illuminate\Database\Eloquent\Model; 

class Tank extends Model
{ 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'tank_type', 'stored_fuel', 'liter'
    ];

    public function dispenser()
    {
          return $this->belongsTo('App\Dispenser');
       
    }

    public function stationtransaction()
    {
        return $this->hasOne('App\StationTransaction');
    }



    /**
     * Get the  name.
     *
     * @param  string  $value
     * @return string
     */
       public function getNameAttribute($value)
    {
        return ucfirst($value);
    }

    public function getTankType()
    {
        return $this->tank_type;
    }

    public function getLiters()
    {
        return $this->liter;
    } 

    public function setLiters($liter)
    {
       $this->liter = $liter;
    } 

        /**
     * Set the liter.
     *
     * @param  string  $value
     * @return void
     */
    public function setLiterAttribute($value)
    {
        $this->attributes['liter'] = ($value);
    }



    public function getStoredFuel()
    {
        return $this->stored_fuel;
    }


    /**
        this method will return the day form created_at field 

     */
    public function getCurrentDay()
    {
        
    return $this->created_at->format('d');
    }



    public function currentVolumn($previousVolunm, $currentVolumn)
    {
 
        if ($currentVolumn < $previousVolunm) {

         return true;

        }

        return false;
    }


}