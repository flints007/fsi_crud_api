<?php

namespace App;

use Illuminate\Http\Request;
use App\Tank;
use App\StationTransaction;
use App\Http\Resources\StationTransactionResource;
 

class DeliveryHelper 
{ 
 
 
    public static function updateVolumeInTank($previous_vol_lfet, $input)
    {

    	$tank_id = $previous_vol_lfet->tank_id;
    	$refill_vol = $input['new_product'];
    	$user_id = $input['user_id'];
    	$tank_id = $input['tank_id'];
    	$vol_sold_by_dispenser = $input['vol_sold_by_dispenser'];
    	$transaction_type = $input['transaction_type'];

		$tank = Tank::findOrFail($tank_id);
    	

  	
    	$vol_left = ((int)$tank->getLiters() - (int)$vol_sold_by_dispenser); // get the available vol before refilling product in the Tank
		

        $new_vol_left = (int)$refill_vol + (int)$tank->getLiters();  // adding new Delivery vol to the previous vol in Tank  
 
		
		$tank = DeliveryHelper::setNewVol($tank_id, $new_vol_left); // update Tank with new vol

     	
 		$transaction = new StationTransaction;
        $transaction->user_id = $user_id;
        $transaction->tank_id = $tank_id;
        $transaction->vol_sold_by_dispenser = $new_vol_left; // add new vol
        $transaction->transaction_type = $transaction_type;
        $transaction->before_delivery = $vol_left;  // this 
        $transaction->save();
 
 
      // return StationTransactionResource::collection($transaction);
 	 // return $this->sendResponse($transaction->toArray(), 'Trasaction updated successfully.');
    }

	public static function setNewVol($tank_id, $new_vol_left)
	{
		   	$tank = Tank::findOrFail($tank_id);
	        $tank->setLiterAttribute($new_vol_left);
	        $tank->save();

	        return $tank;
	}



}
