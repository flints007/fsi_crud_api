<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class StationTransactionResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
            return [
            
                    'id' => $this->id,
                    'user_id' => $this->user_id,
                    'tank_id' => $this->tank_id, 
                    'vol_left_in_tank' => $this->vol_left_in_tank,
                    'vol_sold_by_dispenser' => $this->vol_sold_by_dispenser, 
                    'transaction_type' =>$this->transaction_type,
                    'new_product' => $this->vol_left_in_tank,

         
            ];
    }
}
