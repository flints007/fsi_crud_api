<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class DispenserResource extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'name' => $this->name,
            'hs_code' => $this->hs_code,
            'model_no' => $this->model_no, 
            'magnitude_of_discharge' => $this->magnitude_of_discharge,
            'operation_mode' => $this->operation_mode,
            'oil_transportation_mode' =>$this->oil_transportation_mode,

 
        ];
    }
}
