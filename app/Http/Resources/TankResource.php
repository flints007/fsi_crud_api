<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class TankResource extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);//

        return [

           'id' => $this->id,
           'name' => $this->name,
            'tank_type' => $this->tank_type, 
            'liter' => $this->liter,
            'stored_fuel' => $this->stored_fuel,
            'dispenser_id' =>$this->dispenser_id,
        
        ];
    }
}
