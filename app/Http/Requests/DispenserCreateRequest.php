<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DispenserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'hs_code' => 'required',
            'model_no' => 'required', 
            'magnitude_of_discharge' => 'required',
            'operation_mode' => 'required',
            'oil_transportation_mode' => 'required', 
        ];


    }

 
}
