<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request; 
use App\Http\Requests\TankCreateRequest;
use App\Http\Resources\TankResource;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Tank;
use App\Dispenser;
use Validator;


class TankController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tanks = Tank::all();

       // return TankResource::collection($tanks);
      return $this->sendResponse($tanks->toArray(), 'Tanks retrieved successfully.');
        
    }

 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *  
     */
    public function store(Request $request) //TankCreateRequest to be added later
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required',
            'tank_type' => 'required', 
            'liter' => 'required',
            'stored_fuel' => 'required', 
            ]);
 


        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $name = $request->name;
        $tank_type = $request->tank_type;
        $liter = $request->liter;
        $stored_fuel = $request->stored_fuel;

        $tank = Tank::firstOrNew(['name' =>$name]);
        $tank->name = $name;
        $tank->tank_type = $tank_type;
        $tank->liter = $liter;
        $tank->stored_fuel = $stored_fuel; 

        $dispenser = Dispenser::find($request->dispenser_id);
        $dispenser->tank()->save($tank);
  
      
        return $this->sendResponse($tank->toArray(), 'Tank created successfully.');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { 
 
        $tank = Tank::findOrFail($id); 

        if (!$tank) {
            return $this->sendError('Tank not Found. ');
        }

    return $this->sendResponse($tank->toArray(), 'Tank retrived successfully. ');
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tank $tank)
    {
        $input = $request->all();

        $validator = Validator::make($input, [ 
            'name' => 'required',
            'tank_type' => 'required', 
            'liter' => 'required',
            'stored_fuel' => 'required', 
            ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $tank->name = $input['name'];
        $tank->tank_type = $input['tank_type'];
        $tank->liter = $input['liter'];
        $tank->stored_fuel = $input['stored_fuel'];
        $tank->save();

        return $this->sendResponse($tank->toArray(), 'Tank updated successfully.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tank $tank)
    {
        $tank->delete();

        return $this->sendResponse($tank->toArray(), 'Tank deleted successfully.');
    }

 



}
