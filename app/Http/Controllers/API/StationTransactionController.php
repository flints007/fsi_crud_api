<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Http\Request;
use App\Tank;
use App\StationTransaction;
use Validator;
use App\User;
use DB;
use App\DeliveryHelper;

class StationTransactionController extends BaseController
{
  
 

    public function store(Request $request)
    {
        $input = $request->all(); 
        $vol_sold_by_dispenser = $input['vol_sold_by_dispenser'];
        $transaction_type = $input['transaction_type'];

        $validator = Validator::make($input, [ 
            'user_id' => 'required',
            'tank_id' => 'required',  
            'vol_sold_by_dispenser' => 'required', 
            'transaction_type' => 'required',
            ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }


         //check if Transaction was created for this day

        $check_transaction_to_day = DB::table('station_transactions')->whereDay('created_at', '=', date('d'))->first(); 

       
     
       if ($check_transaction_to_day == null && $transaction_type == config('constants.DELIVERY')) { 
             DeliveryHelper::updateVolumeInTank($this->previous_vol_lfet(), $input);   // taking in the previous days vol and current request 
 
        }elseif ($check_transaction_to_day != null && $transaction_type == config('constants.DELIVERY')) {
             DeliveryHelper::updateVolumeInTank($this->previous_vol_lfet(), $input);   // taking in the previous days vol and current request 
        }else{
              return $this->sendError('Transaction already made for this Day');
        } 
 

        //Validate that the current volume left is < (less than) the previous volume left 

      $val = StationTransaction::validateCurrentVolumnISLessThanPrevious($vol_sold_by_dispenser, $transaction_type);
 
        
        if (!$val && $transaction_type == config('constants.END DAY'))
            return $this->sendError('current volume left must < (less than) the previous volume left except there is a delivery into the tank.');
 

        $transaction = new StationTransaction;
        $transaction->user_id = $input['user_id'];
        $transaction->tank_id = $input['tank_id'];
        $transaction->vol_sold_by_dispenser = $input['vol_sold_by_dispenser'];

        
        /**
        //Upload (daily) the volume (in Liters) of product left in each tank at the station 

        To upload the volume of product left in each tank,

        1. I have to get the previous vol of product available in the tank
        2. then subtrack the vol_sold_by_dispenser from the previous vol
        3. the result will the be vol_left_in_tank

        Note: the vol_left_in_tank will not be manually entered

        Upload (daily) the volume (in Liters) of product sold by each Dispenser at the station.

        As each tank is physically mapped to each Dispenser both product sold by each Dispenser and 
        product left in each tank will be uploaded at once probably.

        */
 
        $tank = Tank::findOrFail($input['tank_id']);
 

        //get the current vol of product available in Tank
        //get the vol_sold_by_dispenser assuming this dispense is attached to this tank in question.


        $vol_left = $this->computeVolLeftInTank($tank->getLiters(), $input['vol_sold_by_dispenser']); 
   

        $transaction->vol_left_in_tank = $vol_left;   // save the vol_left in the db


        $tank->setLiterAttribute($vol_left);          // adjust the volume in Tank
        $tank->save(); 

    
         
        $user = User::findOrFail($input['user_id']); // get the current user
      
        $user->stationtransaction()->save($transaction); // map user and transaction table 

        $tank->stationtransaction()->save($transaction);  //map tank and transaction table


        return $this->sendResponse($transaction->toArray(), 'Trasaction updated successfully.');

    }



   /**
        this method will return the day form created_at field 
     */
    public function computeVolLeftInTank($previousVol, $volSold)
    {
         
    return (int)$previousVol - (int)$volSold; // typecasting; converting string to int

    }






 

}
 