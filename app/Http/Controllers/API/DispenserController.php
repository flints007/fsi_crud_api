<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\DispenserCreateRequest;
use App\Http\Resource\DispenserResource;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Dispenser;
use Validator;

class DispenserController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dispenser = Dispenser::get();

        return $this->sendResponse($dispenser->toArray(), 'Dispenser retrieved successfully.');
    }

 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
 

       public function store(DispenserCreateRequest $request) 
       {
        
        $input = $request->all();  

        $dispenser = Dispenser::firstOrNew(['name' => $input['name'] ]); // to prevent duplicate I used fistOrNew instead of  Dispenser::create($input)
        
        $dispenser->hs_code = $input['hs_code'];
        $dispenser->model_no = $input['model_no'];
        $dispenser->magnitude_of_discharge = $input['magnitude_of_discharge'];
        $dispenser->operation_mode = $input['operation_mode'];
        $dispenser->oil_transportation_mode = $input['oil_transportation_mode'];
        $dispenser->save();

        return $this->sendResponse($dispenser->toArray(), 'Dispenser created successfully.');

        }

        
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dispenser = Dispenser::findOrFail($id);
 
        if (!$dispenser) {
            return $this->sendError('dispenser not Found. ');
        }

        return $this->sendResponse($dispenser->toArray(), 'Dispenser retrived successfully. ');
    }

 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dispenser $dispenser)
    {
 

        $input = $request->all();

        $validator = Validator::make($input, [  
            'hs_code' => 'required',
            'model_no' => 'required', 
            'magnitude_of_discharge' => 'required',
            'operation_mode' => 'required',
            'oil_transportation_mode' => 'required', 
            ]);

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $dispenser->hs_code = $input['hs_code'];
        $dispenser->model_no = $input['model_no'];
        $dispenser->magnitude_of_discharge = $input['magnitude_of_discharge'];
        $dispenser->operation_mode = $input['operation_mode'];
        $dispenser->oil_transportation_mode = $input['oil_transportation_mode'];
        $dispenser->save();

        return $this->sendResponse($dispenser->toArray(), 'Dispenser updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dispenser $dispenser)
    {
        $dispenser->delete();

        return $this->sendResponse($dispenser->toArray(), 'Dispenser deleted successfully.');
    }

 
}
