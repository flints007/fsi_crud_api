<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dispenser extends Model
{
    //
    protected $fillable = [

        'name', 'hs_code', 'model_no', 'magnitude_of_discharge', 'operation_mode', 'oil_transportation_mode',
    ];


    public function tank()
    {
        return $this->hasOne('App\Tank');
    }


}
