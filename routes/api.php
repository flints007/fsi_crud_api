<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
| 
*/
Route::post('register', 'API\RegisterController@register'); 
Route::get('login', 'API\LoginController@login'); 

	Route::group(['prefix' => 'v1' , 'middleware' => 'auth:api' ], function () {
	Route::resource('tanks', 'API\TankController');
	Route::apiResource('dispensers', 'API\DispenserController');

	Route::resource('transactions', 'API\StationTransactionController');

});
 